<?php

$api = app(\Dingo\Api\Routing\Router::class);

$api->version(env('API_VERSION', 'v1'), function ($api) {
    $api->post('/login', 'App\Api\Controllers\Auth\ApiAuthController@login');

    $api->group(['middleware' => 'api.auth'], function ($api) {
        $api->get('customer/', 'App\Api\Controllers\ApiCustomerController@index');
        $api->post('customer/', 'App\Api\Controllers\ApiCustomerController@store');

        $api->resource('transaction', 'App\Api\Controllers\ApiTransactionController');
        $api->get('transaction/{customerId}/{transactionId}', 'App\Api\Controllers\ApiTransactionController@showByCustomer');
    });

});
