<?php

use Illuminate\Database\Seeder;

class CustomersTransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('customers')->truncate();
        DB::table('transactions')->truncate();

        factory(App\Customer::class, 20)->create()->each(function ($c) {
            $c->transactions()->saveMany(factory(App\Transaction::class, rand(1,10))->make());
        });

    }
}
