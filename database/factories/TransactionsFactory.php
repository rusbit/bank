<?php

use Faker\Generator as Faker;

$factory->define(App\Transaction::class, function (Faker $faker) {
    return [
        'amount' => rand(10, 9999),
        'created_at' => rand(2000, 2018) . '-' . rand(1, 12) . '-' . rand(1, 28) . ' 14:00:00'
    ];
});
