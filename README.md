# README #

**Server Requirements**
+ see Laravel 5.7 server requirements

**Instalation:**
+ `composer update`
+ copy settings from *.env.example* to *.env*
+ create database
+ configure *.env*
+ `php artisan key:generate`
+ `php artisan migrate`

**Optional seeding database with test customers and transactions**
+ `composer dump-autoload`
+ `php artisan db:seed`

**API**
+ documentation - public/api_documentation.md
+ api url - your_domain/api/

**CRON scripts**
+ Start point for CRON - App\Console\Kernel schedule()
+ path to file with transactions sum - storage/app/public/transactions_sum.txt

