FORMAT: 1A

# Bank API

# Authentication

## Login as user [POST /login]
Returns JWT token on successful authentication.

+ Request (application/json)
    + Body

            {
                "email": "test@test.com",
                "password": "testpassword"
            }

+ Response 200 (application/json)
    + Body

            {
                "token": "<JWT>"
            }

+ Response 422 (application/json)
    + Body

            {
                "error": "Credentials error"
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error"
            }

# Customer [/customer]
Customer resource representation.

## Customers list [GET /customer]
Get a JSON representation of customers.

+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>

+ Response 200 (application/json)
    + Body

            [
                {
                    "customerId": 1,
                    "name": "Nicole Toy",
                    "cnp": "adsas8d8asdgy7"
                },
                {
                    "customerId": 2,
                    "name": "Felicita Hirthe",
                    "cnp": "adsas877asdgy7"
                }
            ]

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header."
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }

## Store customer [POST /customer]


+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>
    + Body

            {
                "name": "Nicole Toy",
                "cnp": "adsas8d8asdgy7"
            }

+ Response 200 (application/json)
    + Body

            {
                "customerId": 1
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header"
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "422 Unprocessable Entity",
                "errors": {
                    "cnp": "The cnp field is required",
                    "name": "The name field is required"
                }
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }

# Transaction [/transaction]
Transaction resource representation.

## Transactions list [GET /transaction/?customerId=1&date=14.10.2108&amount=25.50&offset=2&limit=3]
Get a transactions list by filters.

+ Parameters
    + customerId: (string, optional) - filter by customer id
    + amount: (string, optional) - filter by amount
    + date: (string, optional) - filter by transaction date
    + offset: (string, optional) - get transactions query offset
    + limit: (string, optional) - get transactions query limit

+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>

+ Response 200 (application/json)
    + Body

            [
                {
                    "transactionId": 1,
                    "customerId": 2,
                    "amount": 120.88,
                    "date": "20.03.2015"
                },
                {
                    "transactionId": 2,
                    "customerId": 3,
                    "amount": 10,
                    "date": "21.03.2015"
                }
            ]

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header."
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }

## Store transaction [POST /transaction]


+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>
    + Body

            {
                "customerId": 1,
                "amount": "25.50"
            }

+ Response 200 (application/json)
    + Body

            {
                "transactionId": 1,
                "customerId": 1,
                "amount": 25.5,
                "date": "21.03.2015"
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header."
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "422 Unprocessable Entity",
                "errors": {
                    "customerId": "The customerId field is required",
                    "amount": "The amount field is required"
                }
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }

## Show transaction [POST /transaction/{customerId}/{transactionId}]


+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>

+ Response 200 (application/json)
    + Body

            {
                "transactionId": 1,
                "customerId": 1,
                "amount": 25.5,
                "date": "21.03.2015"
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header."
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }

## Update transaction [PATCH /transaction/{transactionId}]


+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>
    + Body

            [
                {
                    "transactionId": 1,
                    "amount": 25.5
                }
            ]

+ Response 200 (application/json)
    + Body

            {
                "transactionId": 1,
                "customerId": 1,
                "amount": 25.5,
                "date": "21.03.2015"
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header."
            }

+ Response 422 (application/json)
    + Body

            {
                "message": "422 Unprocessable Entity",
                "errors": {
                    "amount": [
                        "The amount field is required."
                    ]
                }
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }

## Delete transaction [DELETE /transaction/{transactionId}]


+ Request (application/json)
    + Headers

            Authorization: Bearer <JWT>

+ Response 200 (application/json)
    + Body

            {
                "status": "success/fail"
            }

+ Response 401 (application/json)
    + Body

            {
                "message": "Failed to authenticate because of bad credentials or an invalid authorization header."
            }

+ Response 500 (application/json)
    + Body

            {
                "error": "Server side error message"
            }