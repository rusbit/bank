@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center"><b>CUSTOMERS TRANSACTIONS</b></h3>
                </div>

                <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Customer ID</th>
                                <th>Amount</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($transactions as $transaction)
                            <tr>
                                <td>{{$transaction->id}}</td>
                                <td>{{$transaction->customer_id}}</td>
                                <td>{{$transaction->amount}}</td>
                                <td>{{$transaction->created_at->format('d.m.Y')}}</td>
                            </tr>
                                @endforeach
                            </tbody>
                        </table>
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
