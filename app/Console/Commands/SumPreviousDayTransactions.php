<?php

namespace App\Console\Commands;

use App\Transaction;
use Illuminate\Console\Command;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class SumPreviousDayTransactions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transaction:sum';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates the amount of transactions for the previous day';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sum = Transaction::whereDate('created_at', Carbon::now()->addDay(-1))->sum('amount');

        try {
            Storage::disk('public')->append(
                'transactions_sum.txt',
                date('Y-m-d') . ' => ' . $sum
            );
            $this->info($sum);
        } catch (\Exception $exception) {
            $this->error($exception->getMessage());
        }

    }
}
