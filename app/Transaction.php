<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ['customer_id', 'amount'];

    /**
     * Get the customer that owns the transaction.
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public static function byFilters($filters)
    {
        $transactions = self::when(isset($filters['customerId']), function ($query) use ($filters) {
            return $query->where('customer_id', $filters['customerId']);
        })
            ->when(isset($filters['amount']), function ($query) use ( $filters) {
                return $query->where('amount', $filters['amount']);
            })
            ->when(isset($filters['date']), function ($query) use ( $filters) {
                return $query->whereDate('created_at', date('Y-m-d', strtotime($filters['date'])));
            })
            ->when((isset($filters['offset']) && isset($filters['limit'])), function ($query) use ( $filters) {
                $query->offset($filters['offset'])->limit($filters['limit']);
            })
            ->get();

        return $transactions;
    }
}
