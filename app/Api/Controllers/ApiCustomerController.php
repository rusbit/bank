<?php

namespace App\Api\Controllers;

use App\Customer;
use App\Api\Requests\StoreCustomerRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\Customer as CustomerResource;

/**
 * Customer resource representation.
 *
 * @Resource("Customer", uri="/customer")
 */
class ApiCustomerController extends Controller
{
    /**
     * Customers list
     *
     * Get a JSON representation of customers.
     *
     * @Get("/")
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}),
     *     @Response(200, body={{"customerId":1, "name":"Nicole Toy", "cnp":"adsas8d8asdgy7"},
     *     {"customerId":2, "name":"Felicita Hirthe", "cnp":"adsas877asdgy7"}}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header."}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     */
    public function index()
    {
        return response()->json(CustomerResource::collection(Customer::all()), 200);
    }


    /**
     * Store customer
     *
     * @Post("/")
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}, body={"name":"Nicole Toy", "cnp":"adsas8d8asdgy7"}),
     *     @Response(200, body={"customerId":1}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header"}),
     *     @Response(422, body={"message": "422 Unprocessable Entity", "errors":{"cnp":"The cnp field is required",
     *     "name":"The name field is required"}}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     *
     * @param Request $request
     */
    public function store(StoreCustomerRequest $request)
    {
        $customer =  Customer::create($request->all());

        return response()->json(['customerId' => $customer->id], 200);
    }
}
