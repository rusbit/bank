<?php

namespace App\Api\Controllers\Auth;

use Dingo\Api\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * @Resource("Authentication")
 */
class ApiAuthController
{

    /**
     * Login as user
     *
     * Returns JWT token on successful authentication.
     *
     * @param Request $request
     * @return array
     *
     * @Post("/login")
     * @Transaction({
     *     @Request({"email": "test@test.com", "password": "testpassword"}),
     *     @Response(200, body={"token": "<JWT>"}),
     *     @Response(422, body={"error":"Credentials error"}),
     *     @Response(500, body={"error":"Server side error"})
     * })
     */
    public function login(Request $request)
    {
        try {
            if (! $token = JWTAuth::attempt($request->only('email', 'password'))) {
                return response()->json(['error' => 'User with this email or password does not exists.'], 422);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'Could not create token!'], 500);
        }

        return response()->json(compact('token'),200);
    }
}
