<?php

namespace App\Api\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Transaction as TransactionResource;
use Dingo\Api\Exception\StoreResourceFailedException;
use App\Api\Requests\StoreTransactionRequest;
use App\Api\Requests\UpdateTransactionRequest;

/**
 * Transaction resource representation.
 *
 * @Resource("Transaction", uri="/transaction")
 */
class ApiTransactionController extends Controller
{
    /**
     * Transactions list
     *
     * Get a transactions list by filters.
     *
     * @Get("/?customerId=1&date=14.10.2108&amount=25.50&offset=2&limit=3")
     * @Parameters({
     *      @Parameter("customerId", description="filter by customer id"),
     *      @Parameter("amount", description="filter by amount"),
     *      @Parameter("date" , description="filter by transaction date"),
     *      @Parameter("offset", description="get transactions query offset"),
     *      @Parameter("limit", description="get transactions query limit")
     * })
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}),
     *     @Response(200, body={{"transactionId":1, "customerId":2, "amount":120.88, "date":"20.03.2015"},
     *     {"transactionId":2, "customerId":3, "amount":10.00, "date":"21.03.2015"}}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header."}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     */
    public function index(Request $request)
    {
        //maybe limit results

        $transactions = Transaction::byFilters($request->only(['customerId', 'amount', 'date', 'offset', 'limit']));

        return response()->json(TransactionResource::collection($transactions), 200);
    }

    /**
     * Store transaction
     *
     * @Post("/")
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}, body={"customerId":1, "amount":"25.50"}),
     *     @Response(200, body={"transactionId":1, "customerId":1, "amount":25.50, "date":"21.03.2015"}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header."}),
     *     @Response(422, body={"message": "422 Unprocessable Entity", "errors":{"customerId":"The customerId field is required",
     *     "amount":"The amount field is required"}}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     *
     * @param Request $request
     */
    public function store(StoreTransactionRequest $request)
    {
        $transaction = Transaction::create(['customer_id' => $request->customerId, 'amount' => $request->amount]);

        return response()->json([
            'transactionId' => $transaction->id,
            'customerId' => $request->customerId,
            'amount' => number_format($request->amount, 2, '.', ''),
            'date' => $transaction->created_at->format('d.m.Y')
        ], 200);
    }

    /**
     * Show transaction
     *
     * @Post("/{customerId}/{transactionId}")
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}),
     *     @Response(200, body={"transactionId":1, "customerId":1, "amount":25.50, "date":"21.03.2015"}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header."}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showByCustomer(Request $request)
    {
        //APP_URL/transaction/{customerId}/{transactionId} - ?

        $transaction = Transaction::where([
            'id' => $request->transactionId,
            'customer_id' => $request->customerId
        ])
            ->first();

        if(!$transaction){
            throw new StoreResourceFailedException(
                'Error, transaction not found'
            );
        }

        return response()->json([
            'transactionId' => $transaction->id,
            'customerId' => $transaction->customer_id,
            'amount' => $transaction->amount,
            'date' => $transaction->created_at->format('d.m.Y')
        ], 200);
    }


    /**
     * Update transaction
     *
     * @Patch("/{transactionId}")
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}, body={{"transactionId":1, "amount": 25.50}}),
     *     @Response(200, body={"transactionId":1, "customerId":1, "amount":25.50, "date":"21.03.2015"}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header."}),
     *     @Response(422, body={"message": "422 Unprocessable Entity", "errors":{
     *         "amount":{"The amount field is required."}}}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     */
    public function update(UpdateTransactionRequest $request, $id)
    {
        $transaction = Transaction::whereId($id)->first();
        if(!$transaction){
            throw new StoreResourceFailedException(
                'Error, transaction not found'
            );
        }
        $transaction->update(['amount' => $request->amount]);

        return response()->json([
            'transactionId' => $transaction->id,
            'customerId' => $transaction->customer_id,
            'amount' => $transaction->amount,
            'date' => $transaction->updated_at->format('d.m.Y')
        ], 200);
    }

    /**
     * Delete transaction
     *
     * @Delete("/{transactionId}")
     * @Transaction({
     *     @Request(headers={"Authorization": "Bearer <JWT>"}),
     *     @Response(200, body={"status": "success/fail"}),
     *     @Response(401, body={"message": "Failed to authenticate because of bad credentials or an invalid authorization header."}),
     *     @Response(500, body={"error":"Server side error message"})
     * })
     * @Versions({"v1"})
     */
    public function destroy($id)
    {
        $status = Transaction::whereId($id)->delete();

        return response()->json(['status' => $status ? 'success' : 'fail'], 200);
    }
}
