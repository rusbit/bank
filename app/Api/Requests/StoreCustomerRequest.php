<?php

namespace App\Api\Requests;

use Dingo\Api\Http\FormRequest;

class StoreCustomerRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  => 'required|max:50',
            'cnp' => 'required|max:255',
        ];
    }
}
