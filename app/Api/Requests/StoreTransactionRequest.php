<?php

namespace App\Api\Requests;

use Dingo\Api\Http\FormRequest;

class StoreTransactionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customerId'  => 'required|exists:customers,id',
            'amount' => 'required|numeric|min:1|max:999999',
        ];
    }

    public function messages()
    {
        return [
            'customerId.exists' => 'customerId must present in customers table',
            'customerId.required' => 'customerId field is required'
        ];
    }
}
