<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Transaction extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'transactionId' => $this->id,
            'customerId' => $this->customer_id,
            'amount' => $this->amount,
            'date' => $this->created_at->format('d.m.Y')
        ];
    }
}
